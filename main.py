from tkinter import *
import random
import time
import datetime
import threading


class View:
    def __init__(self, controller):
        self.controller = controller

        # configure root
        self.root = Tk()
        self.view_height = self.root.winfo_screenheight() // 2
        self.view_width = self.root.winfo_screenwidth() // 2
        self.root.geometry(f'{self.view_width}x{self.view_height}+'
                           f'{self.view_width - self.view_width // 2}+{self.view_height - self.view_height // 2}')
        self.root.resizable(height=False, width=False)
        self.root.title('Speed Typing Test')
        self.root.config(bg='#0C0C0C')

        # label territory
        self.app_name = Label(self.root,
                              text='Speed Typing Test',
                              bg='#0C0C0C',
                              fg='#ffa500',
                              font=('OpenSymbol', 15, 'bold'))

        self.text_lbl = Label(self.root,
                              justify=CENTER,
                              width=85,
                              bg='#0C0C0C',
                              fg='#C39EA0',
                              font=('OpenSymbol', 11, 'bold'))

        self.hint_lbl = Label(self.root,
                              justify=CENTER,
                              width=113,
                              text='Click on entry',
                              bg='#0C0C0C',
                              fg='#FA255E',
                              font=('OpenSymbol', 9, NORMAL))

        self.mistake_lbl = Label(self.root,
                                 justify=CENTER,
                                 bg='#0C0C0C',
                                 fg='#FA255E',
                                 font=('OpenSymbol', 11, 'bold'))

        self.timer_lbl = Label(self.root,
                               justify=CENTER,
                               bg='#0C0C0C',
                               fg='#F8E5E5',
                               font=('OpenSymbol', 12, 'bold'))

        self.accuracy_lbl = Label(self.root,
                                  justify=CENTER,
                                  bg='#0C0C0C',
                                  fg='#FA255E',
                                  font=('OpenSymbol', 11, 'bold'))

        # entry territory
        self.typing_entry = Entry(self.root,
                                  justify=LEFT,
                                  width=79,
                                  bg='#191919',
                                  fg='#F8E5E5',
                                  bd=0,
                                  highlightthickness=2,
                                  font=('OpenSymbol', 11, 'bold'))

        # button territory
        self.reset_btn = Button(self.root,
                                justify=CENTER,
                                bg='#C39EA0',
                                fg='#000000',
                                activebackground='#af8e90',
                                bd=0,
                                highlightthickness=0,
                                font=('OpenSymbol', 12, 'bold'),
                                text='Reset',
                                command=self.controller.reset)

        self.typing_entry.bind('<Return>', self.controller.accept)
        self.typing_entry.bind('<FocusIn>', self.controller.focus)

        self.app_name.place(x=self.view_width * 7 / 20, y=self.view_height / 10)
        self.text_lbl.place(x=0, y=self.view_height / 4)
        self.hint_lbl.place(x=0, y=self.view_height * 6 / 10)
        self.mistake_lbl.place(x=self.view_width / 5, y=self.view_height * 3 / 4)
        self.timer_lbl.place(x=self.view_width / 2 - 35, y=self.view_height * 3 / 4 - 3)
        self.accuracy_lbl.place(x=self.view_width * 23 / 30, y=self.view_height * 3 / 4)
        self.typing_entry.place(x=20, y=self.view_height / 2)

    def show_reset_btn(self):
        self.reset_btn.place(x=self.view_width / 2 - 35, y=self.view_height * 7 / 8)


class Controller:
    text_samples = ['Becoming a better writer takes practice, and you are already practicing',
                    'Make sure you are clear on the concepts you’re writing about.',
                    'Improving writing involves putting yourself in your readers’ shoes',
                    'Prepositions are not difficult to understand.',
                    'Literary greats can write long, complex sentences with flair. Why not you?']

    def __init__(self):
        self.view = View(self)
        self.root = self.view.root

        self.text_lbl = self.view.text_lbl
        self.hint_lbl = self.view.hint_lbl
        self.mistake_lbl = self.view.mistake_lbl
        self.timer_lbl = self.view.timer_lbl
        self.accuracy_lbl = self.view.accuracy_lbl
        self.typing_entry = self.view.typing_entry
        self.reset_btn = self.view.reset_btn

        self.selected_sample = None
        self.start_time = None
        self.stop = False

        self.root.mainloop()

    def start(self):
        self.typing_entry.configure(state=DISABLED)
        number_of_sample = len(self.text_samples) - 1
        self.hint_lbl['text'] = ''
        self.timer_lbl['text'] = '00:03:00'
        self.countdown_timer()
        self.selected_sample = self.text_samples[random.randint(0, number_of_sample)]
        self.typing_entry.configure(state=NORMAL)
        self.text_lbl['text'] = self.selected_sample
        self.hint_lbl['text'] = 'to check press enter'
        self.chronometer()

    def reset(self):
        self.text_lbl['text'] = ''
        self.mistake_lbl['text'] = ''
        self.accuracy_lbl['text'] = ''
        self.timer_lbl['text'] = '00:03:00'
        self.typing_entry.delete(0, 'end')
        self.reset_btn.place_forget()
        self.stop = False
        threading.Thread(target=self.start, args=()).start()

    def accept(self, event):
        self.stop = True
        input_text = self.typing_entry.get()
        if input_text == '':
            return
        self.hint_lbl['text'] = ''
        mistake, accuracy = self.check(input_text)
        self.mistake_lbl['text'] = f'Mistakes\n{mistake}'
        self.accuracy_lbl['text'] = f'Accuracy\n{accuracy}'
        self.view.show_reset_btn()

    def countdown_timer(self):
        reminder_time = time.time() + 3
        while True:
            now = time.time()
            if now < reminder_time:
                self.timer_lbl['text'] = datetime.datetime.fromtimestamp(reminder_time - now - 1800).strftime(
                    '%M:%S.%f')[:-3]
            else:
                break

    def chronometer(self):
        self.start_time = time.time()
        while not self.stop:
            now = time.time()
            self.timer_lbl['text'] = datetime.datetime.fromtimestamp(now - self.start_time - 1800).strftime(
                '%M:%S.%f')[:-3]

    def check(self, typed_text):
        mistake = 0

        for text_letter, typed_letter in zip(self.selected_sample, typed_text):
            if text_letter != typed_letter:
                mistake += 1

        typed_text_length = len(typed_text)
        sample_text_length = len(self.selected_sample)

        if typed_text_length != sample_text_length:
            mistake += abs(sample_text_length - typed_text_length)

        accuracy = 100 - round(mistake / sample_text_length, 2) * 100
        accuracy = f'{accuracy:.2f}%'

        return mistake, accuracy

    def focus(self, event):
        threading.Thread(target=self.start, args=()).start()


Controller()
